set nocompatible              " be iMproved, required
filetype off                  " required

"set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'
" Git plugin not hosted on GitHub
Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}

"ColorSheme Dracula
Plugin 'zenorocha/dracula-theme'

"Syntaxtic
Plugin 'Syntastic'
Plugin 'bling/vim-airline'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" color 
syntax on
set t_Co=256
set term=xterm
set background=dark

" tab change to space
set expandtab
set shiftwidth=2
set tabstop=2

" number line
hi LineNr ctermfg=grey
hi CursorLine   cterm=NONE ctermbg=DarkGray 
hi CursorColumn cterm=NONE ctermbg=DarkGray
set number
set cursorline

" line around currentline
set scrolloff=7

"command line completion
set wildmenu

" show white space
set list
set listchars=tab:>─,trail:-,nbsp:¤

" Doxygen
let g:load_doxygen_syntax=1

" indent
set autoindent

" color when more than 80 char
match ErrorMsg '\%>80v.\+'

" status line
set laststatus=2

" seach top down auto
set wrapscan

" enable mousse
set mouse=a

set backspace=2



